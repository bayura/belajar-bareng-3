import 'dart:io';

import 'package:belajar_bareng/models/leagues_response.dart';
import 'package:belajar_bareng/models/movies.dart';
import 'package:belajar_bareng/models/teams_response.dart';
import 'package:belajar_bareng/models/tools.dart';
import 'package:belajar_bareng/screens/league_team+page.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Tool> tools = [];
  List<Team> teams = [];
  List<League> leagues = [];
  String name = "Joko";
  Movies movies;
  var url = 'https://api.themoviedb.org/3';
  String basedImageUrl = 'http://image.tmdb.org/t/p/w400';
  bool isLoadTeam = false;

  @override
  void initState() {
    // tools.add(Tool(colors: [Colors.black, Colors.black12], progress: 80, description: "Deskripsi Behance", subTitle: "Behance subtitle", title: "Behance", iconUrl: "", color: Colors.blue));
    // tools.add(Tool(progress: 70, description: "Deskripsi dribbble", subTitle: "Dribbble subtitle", title: "Dribbble", iconUrl: "", color: Colors.purple));
    // tools.add(Tool(progress: 60, description: "Deskripsi Pinterest", subTitle: "Pinterest subtitle", title: "Pinteres", iconUrl: "", color: Colors.red));
    getLeagues();
    super.initState();
  }

  @override
  void dispose() { 
    tools.clear();
    super.dispose();
  }

  removeData(){
    tools.removeAt(tools.length-1);
    setState(() {

    });
  }

  addData(){
    setState(() {
      tools.add(Tool(progress: 60, description: "Deskripsi Pinterest", subTitle: "Pinterest subtitle", title: "Pinteres", iconUrl: "", color: Colors.red));
    });
  }

  getMovieDio()async{
    String getNowPlayingMovie = '/movie/now_playing';
    String apiKey = '30f080c451e323e53f8dfadf4e3f241c';
    String language = 'en-US';
    var params = {
      'api_key' : apiKey,
      'language': language
    };
    Response response = await Dio().get(url+getNowPlayingMovie, queryParameters: params);
    Movies newMovies = Movies.fromJson(response.data);
    movies = newMovies;
    setState(() {
      
    });
  }

  getTeamInPremierLeague(String leagueName)async{
    var params = {
      'l' : leagueName,
    };
    Response response = await Dio().get('https://www.thesportsdb.com/api/v1/json/1/search_all_teams.php', queryParameters: params);
    TeamsResponse teamsResponse = TeamsResponse.fromJson(response.data);
    setState(() {
      isLoadTeam = true;
      this.teams.clear();
      this.teams.addAll(teamsResponse.teams);
    });
  }

  getLeagues()async{
    final response = await Dio().get('https://www.thesportsdb.com/api/v1/json/1/all_leagues.php');
    LeaguesResponse leaguesResponse = LeaguesResponse.fromJson(response.data);
    if(response.statusCode==HttpStatus.ok){
      setState(() {
        leagues.addAll(leaguesResponse.leagues);
      });
    } else {
      print("error happened");
    }
  }

  getMovie()async{
    var url = 'https://api.themoviedb.org/3/';
    String getNowPlayingMovie = 'movie/now_playing';
    String apiKey = '30f080c451e323e53f8dfadf4e3f241c';
    String language = 'en-US';
    var response = await http.get(url+getNowPlayingMovie+'?api_key=$apiKey&language=$language');
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');
    Movies newMovies = moviesFromJson(response.body);
    setState(() {
      
    });
    movies = newMovies;
  }

  postMovie()async{
    var url = 'https://api.themoviedb.org/3';
    var params={
      "id" : 1,
      "name" : 2
    };
    var response = await http.post(url, body: params);
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text("Mine", style: TextStyle(color: Colors.black),),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(Icons.search, color: Colors.black,),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            // Center(
            //   child: RaisedButton(
            //     child: Text("Get Teams"),
            //     onPressed: ()=>getTeamInPremierLeague('German Bundesliga')
            //   ),
            // ),
            // Text("Trending", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),),
            SizedBox(height: 16),
            ListView.builder(
              primary: false,
              shrinkWrap: true,
              itemCount: leagues.length,
              itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                onTap: ()=>Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => LeagueTeamPage(league: leagues[index],)),
                ),
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 4),
                  margin: EdgeInsets.symmetric(vertical: 8, horizontal : 16),
                  decoration: BoxDecoration(
                    border: Border(bottom: BorderSide(color: Colors.grey[300]))
                  ),
                  child: Text(leagues[index].strLeague),
                ),
              );
             },
            ),
            //Movie and Team
            // movies==null && teams.length==0? SizedBox() : isLoadTeam? ListView.builder(
            //   primary: false,
            //   shrinkWrap: true,
            //   itemCount: teams.length,
            //   itemBuilder: (BuildContext context, int index) {
            //   return Container(
            //     padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
            //     child: Row(
            //       children: <Widget>[
            //         Image.network(teams[index].strTeamBadge, width: 60,),
            //         SizedBox(width: 16),
            //         Text(teams[index].strTeam)
            //       ],
            //     ),
            //   );
            //  },
            // ) : ListView.builder(
            //   primary: false,
            //   shrinkWrap: true,
            //   itemCount: movies.results.length,
            //   itemBuilder: (BuildContext context, int index) {
            //   return Padding(
            //     padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4),
            //     child: Row(
            //       children: <Widget>[
            //         Image.network(basedImageUrl+movies.results[index].posterPath, width: 100,),
            //         SizedBox(width: 8),
            //         Expanded(
            //           child: Column(
            //             crossAxisAlignment: CrossAxisAlignment.start,
            //             children: <Widget>[
            //               Text(movies.results[index].originalTitle, style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
            //               Row(
            //                 children: <Widget>[
            //                   Icon(Icons.star, color: Colors.amber,),
            //                   Text(movies.results[index].popularity.toString())
            //                 ],
            //               ),
            //               Text(movies.results[index].overview, maxLines: 3,)
            //             ],
            //           ),
            //         )
            //       ],
            //     ),
            //   );
            //  },
            // ),
            // ListView.builder(
            //   primary: false,
            //   shrinkWrap: true,
            //   itemCount: tools.length,
            //   itemBuilder: (BuildContext context, int index) {
            //   return Content(tool: tools[index],);
            //  },
            // ),
            // ButtonPrimary(
            //   title: "Tambah data",
            //   onClicked: addData,
            // ),
            // RaisedButton(
            //   onPressed: removeData,
            //   child: Text("Hapus data"),
            // )
          ],
        ),
      ),
    );
  }
}