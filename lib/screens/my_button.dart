import 'package:flutter/material.dart';

class ButtonPrimary extends StatelessWidget {
  final String title;
  final Function onClicked;

  const ButtonPrimary({Key key, this.title, this.onClicked}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: RaisedButton(
        onPressed: onClicked,
        child: Text(title, style: TextStyle(color: Colors.white),),
        color: Colors.purple,
      ),
    );
  }
}
