import 'package:belajar_bareng/models/tools.dart';
import 'package:belajar_bareng/screens/second_page.dart';
import 'package:flutter/material.dart';

class Content extends StatelessWidget {
  final Tool tool;

  const Content({Key key, this.tool}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: ()=>
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => SecondPage(tool: tool)),
      ),
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: tool.color
        ),
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(vertical :16.0, horizontal: 8),
              child: Image.asset("lib/assets/icon_behance.png", width: 60,),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(tool.title, style: TextStyle(color: Colors.white, fontSize: 17, fontWeight: FontWeight.bold),),
                  Text(tool.subTitle??'Subtitle is Empty', style: TextStyle(color: Colors.white),),
                  SizedBox(
                    height: 8,
                  ),
                  Text(tool.description??'Description is empty', style: TextStyle(color: Colors.white),),
                ],
              ),
            ),
            Container(
              height: 100,
              padding: EdgeInsets.only(right: 16),
              decoration: BoxDecoration(
                color: tool.color,
                borderRadius: BorderRadius.only(topLeft: Radius.circular(50), bottomLeft: Radius.circular(50)),
                boxShadow: [
                  BoxShadow(
                    color: Colors.blue[800],
                    blurRadius: 8.0, // has the effect of softening the shadow
                    spreadRadius: 1.0, // has the effect of extending the shadow
                    offset: Offset(
                      -10.0, // horizontal, move right 10
                      0.0, // vertical, move down 10
                    ),
                  )
                ],
              ),
              child: Icon(Icons.file_download, color: Colors.white,)
            )
          ],
        ),
      ),
    );
  }
}