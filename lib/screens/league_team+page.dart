import 'package:belajar_bareng/models/leagues_response.dart';
import 'package:belajar_bareng/models/teams_response.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class LeagueTeamPage extends StatefulWidget {
  final League league;

  const LeagueTeamPage({Key key, this.league}) : super(key: key);
  @override
  _LeagueTeamPageState createState() => _LeagueTeamPageState();
}

class _LeagueTeamPageState extends State<LeagueTeamPage> {
  List<Team> teams = [];
  bool isLoading = true;

  @override
  void initState() { 
    super.initState();
    getTeamInPremierLeague(widget.league.strLeague);
  }

  getTeamInPremierLeague(String leagueName)async{
    var params = {
      'l' : leagueName,
    };
    Response response = await Dio().get('https://www.thesportsdb.com/api/v1/json/1/search_all_teams.php', queryParameters: params);
    TeamsResponse teamsResponse = TeamsResponse.fromJson(response.data);
    setState(() {
      teams.clear();
      teams.addAll(teamsResponse.teams);
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.league.strLeague),
      ),
      body: isLoading?Center(child: CircularProgressIndicator()) : ListView.builder(
        itemCount: teams.length,
        itemBuilder: (BuildContext context, int index) {
        return Container(
          padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
          child: Row(
            children: <Widget>[
              Image.network(teams[index].strTeamBadge, width: 60,),
              SizedBox(width: 16),
              Text(teams[index].strTeam)
            ],
          ),
        );
        },
      ) 
    );
  }
}