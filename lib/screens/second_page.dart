import 'package:belajar_bareng/models/tools.dart';
import 'package:flutter/material.dart';

class SecondPage extends StatefulWidget {
  final Tool tool;

  const SecondPage({Key key, this.tool}) : super(key: key);
  @override
  _SecondPageState createState() => _SecondPageState();
}

class _SecondPageState extends State<SecondPage> {

  @override
  void initState() {
    print("initstate");
    super.initState();
  }

  @override
  void dispose() {
    print("dispose");
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text(widget.tool.title??"Title is Empty", style: TextStyle(color: Colors.black),),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(Icons.search, color: Colors.black,),
          )
        ],
      ),
      body: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: 16, left:16, right:16),
            height: MediaQuery.of(context).size.height*0.6,
            decoration: BoxDecoration(
              gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: widget.tool.colors),
              borderRadius: BorderRadius.circular(8)
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Image.network('lib/assets/icon_behance.png', width: 120,),
                ),
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Text(widget.tool.title, style: TextStyle(fontSize: 17, color: Colors.white),),
                ),
                Text(widget.tool.description??"Deskripsi", style: TextStyle(color: Colors.white),),
                SizedBox(height: 32),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal : 16.0, vertical: 24),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Text("70M"),
                          Text("Data")
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          Text("70M"),
                          Text("Data")
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          Text("70M"),
                          Text("Data")
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            alignment: FractionalOffset.center,
          ),
          Container(height: 16, color: Colors.blue[300], width: MediaQuery.of(context).size.width, margin: EdgeInsets.symmetric(horizontal: 32),),
          Container(color: Colors.blue[100], height: 16, width: MediaQuery.of(context).size.width, margin: EdgeInsets.symmetric(horizontal: 48),),
          SizedBox(
            height: 60
          ), 
          RaisedButton(onPressed: (){}, child: Text("Button"),)
        ],
      ),
    );
  }
}