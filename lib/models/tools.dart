import 'dart:ui';

class Tool{
  String name;
  String iconUrl;
  Color color;
  List<Color> colors;
  String title;
  String subTitle;
  String description;
  double progress;

  Tool({this.name, this.iconUrl, this.color, this.title, this.subTitle, this.description, this.colors, this.progress});
}