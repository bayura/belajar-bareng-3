import 'dart:convert';

LeaguesResponse leaguesResponseFromJson(String str) => LeaguesResponse.fromJson(json.decode(str));

String leaguesResponseToJson(LeaguesResponse data) => json.encode(data.toJson());

class LeaguesResponse {
    List<League> leagues;

    LeaguesResponse({
        this.leagues,
    });

    factory LeaguesResponse.fromJson(Map<String, dynamic> json) => LeaguesResponse(
        leagues: List<League>.from(json["leagues"].map((x) => League.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "leagues": List<dynamic>.from(leagues.map((x) => x.toJson())),
    };
}

class League {
    String idLeague;
    String strLeague;
    String strSport;
    String strLeagueAlternate;

    League({
        this.idLeague,
        this.strLeague,
        this.strSport,
        this.strLeagueAlternate,
    });

    factory League.fromJson(Map<String, dynamic> json) => League(
        idLeague: json["idLeague"],
        strLeague: json["strLeague"],
        strSport: json["strSport"],
        strLeagueAlternate: json["strLeagueAlternate"] == null ? null : json["strLeagueAlternate"],
    );

    Map<String, dynamic> toJson() => {
        "idLeague": idLeague,
        "strLeague": strLeague,
        "strSport": strSport,
        "strLeagueAlternate": strLeagueAlternate == null ? null : strLeagueAlternate,
    };
}
